# coding: utf-8
import sys
import time
import threading
import random
import telepot
import datetime
from gpiozero import Servo
from time import sleep
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from picamera import PiCamera

"""
Bot Telegram para hacer fotos, videos y controlar 2 servos
"""

""" Variables """

#Texto
u_id = '139761589' # ID del usuario de Telegram
t_space = ' '
t_f_ico = '📷'
t_mainmenu = '🏠 Menu Principal'
t_timemenu = '⏱ Cuántos segundos de video?'
t_movemenu = '🎦 Mover cámara'
t_foto = '📷 Foto'
t_video = '📹 Video'
t_move = '💠 Mover'
t_move_up = '⬆️'
t_move_down = '⬇️'
t_move_left = '⬅️'
t_move_right = '➡️'
t_move_home = '⏺'
t_5 = '5️⃣'
t_10 = '🔟'
t_30 = '3️⃣0️⃣'

# Rutas carpetas
f_video = '../video/'
f_foto = '../foto/'

# Controladores para que no se haga spam
on_foto = False
on_video = False

# Inicialización de la cámara
camera = PiCamera()
camera.rotation = 180
camera.resolution = (1296, 730)

# Servos
servo1Pos = 0.0 #Positivo izquierda, negativo derecha
servo2Pos = 0.0 #Positivo abajo, negativo arriba
servoHor = 17 #GPIO al que está conectado el servo horizontal
servoVer = 18 #GPIO al que está conectado el servo vertical
# Correccion, max y min PW son los valores que admite el PWD del servo, sacados de una guía
correccion = 0.40
maxPW = (2.0 + correccion) / 1000
minPW = (1.0 - correccion) / 1000
# Inicialización de los servos (GPIO, minPW, maxPW)
servo1 = Servo(servoHor, min_pulse_width=minPW, max_pulse_width=maxPW)
servo2 = Servo(servoVer, min_pulse_width=minPW, max_pulse_width=maxPW)


""" Eventos """

# Se ejecuta cada vez que el bot recibe un mensaje
def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)

    # Si el contenido del mensaje recibido no es texto, salimos
    if content_type != 'text':
    	print(str(content_type))
    	bot.sendMessage(u_id, 'No envie archivos')
        return

    print('Chat:', content_type, chat_type, chat_id, msg['text'])

    # Pasamos el comando recibido a minusculas
    command = msg['text'].lower()

    #Si recibimos el mensaje "menu", abrimos el menú
    if command == 'menu':
        print('Comando Menu')
        m_main()
    elif command == 'holi':
        bot.sendMessage(u_id, 'Con ti go, Pi po')

# Se ejecuta cada vez que recibe un callback_query (botones Inline)
def on_callback_query(msg):
    query_id, from_id, data = telepot.glance(msg, flavor='callback_query')
    print('Callback query:', query_id, from_id, data)

    # Controlamos que no se está haciendo la foto, para que no la haga 2 veces
    if data == 'foto':
        print('Hacer foto')
        if on_foto == False:
            bot.answerCallbackQuery(query_id, text='Haciendo foto...')
            make_photo()
        else:
            bot.answerCallbackQuery(query_id, text='Se está haciendo la foto, espere')
        m_main()

    # Controlamos que no está grabando, para que no lo haga 2 veces
    elif data == 'video':
        print('Menu video')
        if on_video == False:
            m_video()
        else:
            bot.answerCallbackQuery(query_id, text='Se está grabando el video, espere')

    # Ejecutar m_mover para mostrar el menú de mover servos
    elif data == 'move':
        m_mover()

    # Foto del menú de mover los servos
    elif data == 'mFoto':
        print('Foto menu mover')
        if on_foto == False:
            bot.answerCallbackQuery(query_id, text='Haciendo foto...')
            make_photo()
        else:
            bot.answerCallbackQuery(query_id, text='Se está haciendo la foto, espere')
        m_mover()

    # Video de 5 segundos, ejecutamos make_video y le pasamos 5 como parámetro
    elif data == 't5':
        print('Video 5 segundos')
        bot.answerCallbackQuery(query_id, text='Grabando video...')
        make_video(5)
        m_main()

    # Video de 10 segundos, ejecutamos make_video y le pasamos 10 como parámetro
    elif data == 't10':
        print('Video 10 segundos')
        bot.answerCallbackQuery(query_id, text='Grabando video...')
        make_video(10)
        m_main()

    # Video de 30 segundos, ejecutamos make_video y le pasamos 30 como parámetro
    elif data == 't30':
        print('Video 30 segundos')
        bot.answerCallbackQuery(query_id, text='Grabando video...')
        make_video(30)
        m_main()

    # Mover servo arriba, si es mayor que -1, restamos 0.2 y movemos el servo a esa posición
    elif data == 'mUp':
        global servo2Pos
        if servo2Pos > -1:
        	print('Moviendo arriba')
        	servo2Pos -= 0.2
        	servo2.value = servo2Pos
        	print('Servo vertical: ' + str(servo2Pos))
        else:
        	print('No se puede más vertical')
        	bot.answerCallbackQuery(query_id, text='No se puede más')

    # Mover servo abajo, si es menor que 1, sumamos 0.2 y movemos el servo a esa posición
    elif data == 'mDown':
        global servo2Pos
        if servo2Pos < 1:
        	print('Moviendo abajo')
        	servo2Pos += 0.2
        	servo2.value = servo2Pos
        	print('Servo vertical: ' + str(servo2Pos))
        else:
        	print('No se puede más vertical')
        	bot.answerCallbackQuery(query_id, text='No se puede más')

    # Mover servo abajo, si es menor que 1, sumamos 0.2 y movemos el servo a esa posición
    elif data == 'mLeft':
        global servo1Pos
        if servo1Pos < 1:
        	print('Moviendo izquierda')
        	servo1Pos += 0.2
        	servo1.value = servo1Pos
        	print('Servo horizontal: ' + str(servo1Pos))
        else:
        	print('No se puede más horizontal')
        	bot.answerCallbackQuery(query_id, text='No se puede más')

    # Mover servo arriba, si es mayor que -1, restamos 0.2 y movemos el servo a esa posición
    elif data == 'mRight':
        global servo1Pos
        if servo1Pos > -1:
        	print('Moviendo derecha')
        	servo1Pos -= 0.2
        	servo1.value = servo1Pos
        	print('Servo horizontal: ' + str(servo1Pos))
        else:
        	print('No se puede más horizontal')
        	bot.answerCallbackQuery(query_id, text='No se puede más')

    # Mover servo a 0 horizontal y 0 vertical
    elif data == 'mHome':
        global servo1Pos
        global servo2Pos
        print('Moviendo a home')
        servo1Pos = 0.0
        servo2Pos = 0.0
        servo1.value = servo1Pos
        servo2.value = servo1Pos

    # Mostrar el menu home
    elif data == 'home':
        m_main()


""" Funciones """

# Menu principal
def m_main():
    keyboard = InlineKeyboardMarkup(inline_keyboard=[[
        InlineKeyboardButton(text=t_foto, callback_data='foto'),
        InlineKeyboardButton(text=t_video, callback_data='video'),
        InlineKeyboardButton(text=t_move, callback_data='move'),
        ]])
    bot.sendMessage(u_id, t_mainmenu, reply_markup=keyboard)

# Menu video
def m_video():
    keyboard = InlineKeyboardMarkup(inline_keyboard=[[
        InlineKeyboardButton(text=t_5, callback_data='t5'),
        InlineKeyboardButton(text=t_10, callback_data='t10'),
        InlineKeyboardButton(text=t_30, callback_data='t30'),
        ],[
        InlineKeyboardButton(text=t_mainmenu, callback_data='home'),
        ]])
    bot.sendMessage(u_id, t_timemenu, reply_markup=keyboard)

# Menu mover
def m_mover():
    keyboard = InlineKeyboardMarkup(inline_keyboard=[[
        InlineKeyboardButton(text=t_f_ico, callback_data='mFoto'),
        InlineKeyboardButton(text=t_move_up, callback_data='mUp'),
        InlineKeyboardButton(text=t_space, callback_data='s'),
        ],[
        InlineKeyboardButton(text=t_move_left, callback_data='mLeft'),
        InlineKeyboardButton(text=t_move_home, callback_data='mHome'),
        InlineKeyboardButton(text=t_move_right, callback_data='mRight'),
        ],[
        InlineKeyboardButton(text=t_space, callback_data='s'),
        InlineKeyboardButton(text=t_move_down, callback_data='mDown'),
        InlineKeyboardButton(text=t_space, callback_data='s'),
        ],[
        InlineKeyboardButton(text=t_mainmenu, callback_data='home'),
        ]])
    bot.sendMessage(u_id, t_movemenu, reply_markup=keyboard)

# Hacer una foto
def make_photo():
    if on_foto == False:
        global on_foto
        on_foto = True
        print('Haciendo foto...')
        camera.start_preview()
        sleep(1)
        now = datetime.datetime.now().strftime("%d-%m-%Y_%H-%M_%S-%f")
        camera.capture(f_foto + now + '.jpg')
        print('Foto guardada')
        bot.sendPhoto(u_id, photo=open(f_foto + now + ".jpg"), caption='✅ ' + now + '.jpg')
        print('Foto enviada')
        on_foto = False

# Grabar video
def make_video(sec):
    if on_video == False:
        global on_video
        on_video = True
        print('Video de ' + str(sec))
        now = datetime.datetime.now().strftime("%d-%m-%Y_%H-%M_%S-%f")
        camera.start_recording(f_video + now + '.h264')
        camera.wait_recording(sec)
        camera.stop_recording()
        print('Video grabado')
        bot.sendVideo(u_id, video=open(f_video + now + '.h264'), duration=sec, caption='✅ ' + now + '.h264')
        print('Video enviado')
        on_video = False




bot = telepot.Bot('473308663:AAHp8wASIOXg9uPa292XWy-6v9JFKLtkThA')
answerer = telepot.helper.Answerer(bot)

MessageLoop(bot, {'chat': on_chat_message,
                  'callback_query': on_callback_query}).run_as_thread()

print('Escuchando...')

bot.sendMessage(u_id, '✅ Bot encendido y listo')
m_main()

# Mantener el programa en funcionamiento
while 1:
    sleep(5)



#Bot Z '473308663:AAHp8wASIOXg9uPa292XWy-6v9JFKLtkThA'
